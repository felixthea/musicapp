class AlbumsController < ApplicationController

  def index
    @albums = Album.all
    render :index
  end

  def show
    @album = Album.find(params[:id])
    render :show
  end

  def new
    @album = Album.new
    @bands = Band.all
    render :new
  end

  def create
    @album = Album.new(params[:album])
    if @album.save
      redirect_to @album
    else
      flash[:notice] ||= []
      flash[:notice] << "Your album was not created."
      redirect_to new_album_url
    end
  end

  def edit
    @album = Album.find(params[:id])
    @bands = Band.all
    render :edit
  end

  def update
    @album = Album.find(paras[:id])
    @album.update_attributes(params[:album])

    redirect_to @album
  end

  def destroy
    Album.find(params[:id]).destroy

    redirect_to albums_url
  end

end
