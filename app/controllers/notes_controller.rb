class NotesController < ApplicationController

  def new
    @note = Note.new
  end

  def create
    params[:note][:track_id] = params[:track_id]
    params[:note][:user_id] = current_user.id
    @note = Note.create(params[:note])
    if @note.save
      redirect_to @note.track
    else
      flash[:notice] ||= []
      flash[:notice] << "Your note was not created."
      redirect_to @note.track
    end
  end

  def destroy
    note = Note.find(params[:id])
    track = note.track
    Note.find(params[:id]).destroy
    redirect_to track
  end
end
