class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by_credentials(params[:user][:email],params[:user][:password])
    if log_in_user!.nil?
      flash[:notice] ||= []
      flash[:notice] << "Cannot log in"
      redirect_to new_session_url
    else
      redirect_to current_user
    end
  end

  def destroy
    session[:session_token] = nil
    redirect_to new_session_url
  end
end
