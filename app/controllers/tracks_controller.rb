class TracksController < ApplicationController
  def index
    @tracks = Track.find_all_by_album_id(params[:album_id])

    render @tracks
  end

  def show
    @track = Track.find(params[:id])
    @notes = @track.notes
    @note = Note.new

    render :show
  end

  def new
    @track = Track.new
    @albums = Album.all
    render :new
  end

  def create
    @track = Track.new(params[:track])
    if @track.save
      redirect_to track_url(@track.id)
    else
      flash[:notice] ||= []
      flash[:notice] << "Your track was not created."
      redirect_to new_album_track_url
    end
  end

  def edit
    @track = Track.find(params[:id])

    render :edit
  end

  def update
    @track = Track.find(params[:id])
    @track.update_attributes(params[:track])

    redirect_to @track
  end

  def destroy
    @track = Track.find(params[:id])
    @album = @track.album
    @track.destroy

    redirect_to album_url(@album)
  end
end
