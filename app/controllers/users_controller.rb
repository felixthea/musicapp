class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      session[:session_token] = @user.session_token
      msg = UserMailer.welcome_email(@user)
      msg.deliver!
      if log_in_user!.nil?
        redirect_to new_session_url
        flash[:notice] ||= []
        flash[:notice] << "Please activate via email first."
      else
        redirect_to @user
      end
    else
      flash[:notice] ||= []
      flash[:notice] << "Could not sign up"
    end
  end

  def show
    @user = User.find_by_id(params[:id])
  end

  def activate
    user = User.find_by_activation_token(params[:activation_token])
    user.activated = true
    user.save
    redirect_to bands_url
  end
end
