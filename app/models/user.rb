require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :email, :password_digest, :session_token, :password, :activation_token, :activated

  validates :email, :password_digest, :session_token, :activation_token, presence: true
  validates_inclusion_of :activated, :in => [true, false]
  validates :email, uniqueness: true

  before_validation :set_session_token
  before_validation :set_activation_token
  before_validation :set_activated

  has_many(
    :notes,
    class_name: "Note",
    foreign_key: :user_id,
    primary_key: :id
  )

  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end

  def reset_session_token!
    self.session_token = User.generate_session_token
  end

  def password=(secret)
    self.password_digest = BCrypt::Password.create(secret)
  end

  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end

  def self.find_by_credentials(email, secret)
    user = User.find_by_email(email)
    user.is_password?(secret) ? user : nil
  end

  private

  def set_session_token
    self.session_token ||= User.generate_session_token
  end

  def set_activation_token
    self.activation_token ||= User.generate_session_token
  end

  def set_activated
    self.activated ||= false
    true
  end
end
