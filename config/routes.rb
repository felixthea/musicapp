MusicApp::Application.routes.draw do
  resources :users do
    collection do
      get 'activate'
    end
  end

  resource :session

  resources :bands do
    resources :albums, :only => [:index, :new, :create, :edit]
  end

  resources :albums do
    resources :tracks, :only => [:index, :new, :create, :edit]
  end

  resources :tracks do
    resources :notes, :only => [:index, :new, :create, :edit]
  end

  resources :notes, :only => :destroy

end
